import sys
import codecs
import re 
from Unicode_VN import *


sys.stdout = codecs.getwriter('utf_8')(sys.stdout)
sys.stdin = codecs.getreader('utf_8')(sys.stdin)



NgyenAm = ['a', 'e', 'i', 'o', 'u', 'y']
def convert2ascii(a, CODE = 1):
    """
    CODE = 0: telex style
    CODE = 1: VNI style
    """
    i = 2
    res = ''
    while i < len(a)-1:
        # unicode characters always start with '\u' or '\x'
        if a[i] == '\\': 
            i += 1
            if i < len(a)-1 and a[i] == '\\':
                res += '\\'
                i += 1 
            # if code starts by \x, 2 chars follows
            elif i < len(a)-1 and a[i] == 'x': 
                if a[i:i+3] in code_dict:
                    res += code_dict[a[i:i+3]][CODE]
                i += 3 
            # if code starts by \u, 4 chars follows
            elif i < len(a) - 1 and a[i] == 'u': 
                if a[i:i+5] in code_dict:
                    res += code_dict[a[i:i+5]][CODE]
                i += 5
        else:
            res += a[i]
            i+= 1 
    return res 

def convert2ascii_test():
    # f = codecs.open('../vn.txt', encoding='utf-8')
    # f = codecs.open('VNTQcorpus-small.txt', encoding='utf-8')
    f = codecs.open('TruyenKieu2.txt', encoding='utf-8')
    i = -1
    for line in f:
        i += 1
        if i > 50: break 
        str1 = repr(line)
        str1 = str1.replace('\\r', '')
        str1 = str1.replace('\\n', '')
        a =  convert2ascii(str1) # remove \n at the end 
        words = re.split(r'\W',a.lower())
        # count = 0 
        for w in words:
            if w != '' and 'a' <= w[0] <= 'z': 
                # count += 1 
                print w, 
        print ''
        # print words
        # b = a.slit(' ')
        # print a 

def VNI2unicode(s):
    i = 0 
    l = len(s)
    s2 = ''
    while i < len(s):
        j = i + 1
        while j < len(s) and '0' <= s[j] <= '9':
            j += 1
        if j == i + 1:
            s2 += s[i]
        else:
            if s[i:j] in code2_dict:
                s2 += code2_dict[s[i:j]]
            else:
                s2 += s[i:j]
        i = j
    return s2  

def vn_extract(w):
    """
    w: a string without ' ' (space) - a word - 1 chu 

    if w[0] == 'q': 
        a, b, c = vn_extract(w[2:])
        return 'qu', b, c
    special case: gi 
    """
    if w[0:2] == 'gi':
        phuam = 'gi'
        if len(w) == 2: return ('gi', 'i', '0')
        i = 2
        while i < len(w) and not ('a' <= w[i] <= 'z'):
            i += 1
        if i == len(w):
            (p, v, d) = vn_extract(w[1:])
        elif w[i] in ['a', 'e', 'i', 'u', 'o', 'y']:
            (p, v, d) = vn_extract(w[2:])
        else: 
            (p, v, d) = vn_extract(w[1:])
        return ('gi', v, d)
    phuam = ''
    van = ''
    dau = '0' 
    found_nguyenam = False 
    for i in range(len(w)):
        if not found_nguyenam:
            if w[i] in ['a', 'e', 'i', 'o', 'u', 'y']:
                # if w[i] != 'i':
                    phuam = w[:i]
                    found_nguyenam = True 
                    van += w[i]
                # else: 

        elif '0'< w[i] < '6':
            dau = w[i] 
        else:
            van += w[i] 
    return (phuam, van, dau)

def myloadfile(file_name = 'TruyenKieu2.txt'):
    f = codecs.open(file_name, encoding='utf-8')
    return f


def indices(lst, element):
    result = []
    offset = -1
    while True:
        try:
            offset = lst.index(element, offset+1)
        except ValueError:
            return result
        result.append(offset)
    # return result



def print_dict(aDict, items = 50):
    aDict = sorted(aDict.items(), key=lambda x: x[1], reverse = True)
    i = 0
    for i in range(min(len(aDict), items)):        
        # if d[1] > 10:
            # i += 1
            chu = aDict[i]            
            print  str(chu[1])+ ', ' +str(i+1) + '. ', chu[0]

def words_from_line(line):
    str1 = repr(line)
    str1 = str1.replace('\\r', '')
    str1 = str1.replace('\\n', '')
    a =  convert2ascii(str1) # remove \n at the end 
    words = re.split(r'\W',a.lower())
    return words

def vn_extract_test():
    A = ['gi', 'gi2n', 'giu73', 'gia4ng', 'gi2', 'gia4', 'd9u7o71c']
    for a in A:
        print a, vn_extract(a)

# vn_extract_test()

def count_phuam_van_dau(file_name ='TruyenKieu.txt', lines = -1):
    dict_phuam = {}
    dict_van = {}
    dict_dau = {}
    # f = codecs.open('VNTQcorpus-small.txt', encoding='utf-8')
    # f = codecs.open(file_name , encoding='utf-8')
    f = myloadfile(file_name)
    i = -1
    for line in f:
        i += 1
        # if i > 50000: break 
        if lines > - 1 and i > lines:
            break 
        words = words_from_line(line)
        # words = re.split(r'\W',a.lower())
        for w in words:
            if w != '' and 'a' <= w[0] <= 'z':
                # print w, vn_extract(w)
                (phuam, van, dau) = vn_extract(w)
                if phuam in dict_phuam:
                    dict_phuam[phuam] += 1
                else:
                    dict_phuam[phuam] = 1 
                if van in dict_van:
                    dict_van[van] += 1
                else:
                    dict_van[van] = 1
                if dau in dict_dau:
                    dict_dau[dau] += 1
                else:
                    dict_dau[dau] = 1
    # print_dict(dict_phuam)
    # print '\n'
    # print_dict(dict_van, min(1000, len(dict_van)))
    # print '\n'
    # print_dict(dict_dau)
    f.close()
    return (dict_phuam, dict_van, dict_dau)

def dem_capvan_lucbat(file_name='TruyenKieu.txt', lines = -1):
    capvan_dict = {}
    f = codecs.open(file_name, encoding='utf-8')
    i = 0
    six_sentence = False # start with sentence having 6 'chu'
    found_first_sentence  = False 
    for line in f:
        i += 1
        # if i > 5: break 
        if lines > -1 and i > lines: break
        words = words_from_line(line)
        W = []
        for w in words:
            if w != '' and 'a' <= w[0] <= 'z': 
                W += [w] 
        l = len(W)
        # print l 
        if l != 6 and l!=8: continue 

        if l == 6:
            if not found_first_sentence: 
                found_first_sentence = True 
                (_, last_van, _) = vn_extract(W[-1])
                continue
            else: 
                (_, this_van, _) = vn_extract(W[-1])
        else: #8 words 
            (_, this_van, _) = vn_extract(W[-3])
        if last_van < this_van:
            a = (last_van, this_van)
        else:
            a = (this_van, last_van)
        if a in capvan_dict:
            capvan_dict[a] += 1
        else:
            capvan_dict[a] = 1 
            
        (_, last_van, _) = vn_extract(W[-1])
    f.close()
    return capvan_dict   

# capvan_dict = demvan_test()
# print_dict(capvan_dict, 10000)

def demchu_tu(file_name = 'TruyenKieu.txt', numlines = -1):
    Chu = {}
    TuGhep  = {}    
    f = codecs.open(file_name   , encoding='utf-8')
    i = -1
    for line in f:
        i += 1 
        if numlines > -1 and i > numlines   :
            break 
        res2 = words_from_line(line)
        # res2 = re.split(r'\W',res)
        new = True 
        for w in res2: 
            w = w.lower()
            if len(w) == 0 or not('a'<=w[0] <= 'z'):
                new = True 
            else: 
                # print w.lower()
                w = VNI2unicode(w)
                if w in Chu:
                    Chu[w] += 1
                else:
                    Chu[w] = 1 
            # tu ghep
                if new:
                    last = w 
                    new = False 
                else: 
                    word = VNI2unicode(last + ' ' + w )
                    if word in TuGhep:
                        TuGhep[word] += 1
                    else:
                        TuGhep[word] = 1
                    last = w 
    f.close()
    return (Chu, TuGhep)


def group_van(capvan_dict):
    """
    capvan_dict = {('a', 'a'): 10, ('oi', 'uoi'): 20, ...}
    return a dict = {groupid: (list of van(s), number of connections)}
    """
    # pass 
    def root_of(van):
        res = van 
        while parent_dict[res] != res:
            res = parent_dict[res]
        return res 

    group_id = 0 
    group_count_dict = {}
    list_van = []
    group_count_list = []
    group_van_dict = [[]]
    group_count = []
    parent_dict = {}
    size_group = {}
    num_groups = 0 


    for c in capvan_dict: 
        sz = capvan_dict[c]
        # if c[0] in van_dict:
            # if c[1] 
        if c[0] == c[1]:
            if c[0] not in list_van:
                list_van.append(c[0])
                parent_dict[c[0]] = c[0]
                size_group[c[0]] = sz 
            else: 
                # print parent_dict 
                size_group[root_of(c[0])] += sz 
        else: #c[0] != c[1]
            if c[0] not in list_van: 
                list_van.append(c[0])
                if c[1] not in list_van:
                    list_van.append(c[1])
                    parent_dict[c[1]] = c[0]
                    parent_dict[c[0]] = c[0]
                    size_group[c[0]] = sz 
                else: #if c[1] in list_van: 
                    r = root_of(c[1])
                    parent_dict[c[0]] = r 
                    size_group[r] += sz 
            else : #c[0] iin list_van 
                if c[1] not in list_van:
                    list_van.append(c[1])
                    r = root_of(c[0])
                    parent_dict[c[1]] = r 
                    size_group[r] += sz 
                else: 
                    r = root_of(c[0])
                    s = root_of(c[1])
                    if r == s: 
                        size_group[r] += sz 
                    else: #merging 
                        # if size_group[r] < size_group[s]:
                            parent_dict[s] = r 
                            size_group[r] += size_group[s] + sz 
                            del size_group[s] 
                        # else: 
                        #     parent_dict[s] = r 
                        #     size_group[r] += size_group[s] + sz 
                        #     del size_group[s] 
    # print parent_dict 
    print size_group , len(size_group )
    # print_dict(size_group)
    for r in size_group:
        print size_group[r], 
        for van in list_van:
            if root_of(van) == r:
                print van ,
        print ''


    # for c in capvan_dict:
    #     # print c[0], c[1] 
    #     if c[0] not in van_dict and c[1] not in van_dict:
    #         group_id += 1 
    #         group_count += [capvan_dict[c]]
    #     elif c[0] not in van_dict: #c[1] in 

def find_missing_sentence(file_name = 'TruyenKieu.txt', numlines = -1):
    Chu = {}
    TuGhep  = {}    
    f = codecs.open(file_name   , encoding='utf-8')
    i = 0
    for line in f:
        i += 1 
        if numlines > -1 and i > numlines:
            break 
        res = words_from_line(line)
        count = 0 
        for w in res:
            if len(w) != 0 and ('a'<=w[0] <= 'z'):
                count += 1 
        if i%2 == 1:
            if count != 6:
                return i 
        if i %2 == 0:
            if count != 8:
                return (i , count, res)
def test_print(file_name = 'TruyenKieu2.txt', numlines = -1):
    Chu = {}
    TuGhep  = {}    
    f = codecs.open(file_name, encoding='utf-8')
    i = 0
    for line in f:
        i += 1 
        print line,
        if i == 50: break 

def print_sentence_has_word(w, file_name = "TruyenKieu2.txt"):
    f = myloadfile(file_name)
    i = 0 
    count = 0 
    for line in f:
        i += 1 
        words = words_from_line(line)
        # if w in words:
        a = 0 
        n = len(indices(words, w))
        while a < n:
            a +=1 
            count += 1 
            print str(count) + '. (' + str(i) + ') ' + line, 


print_sentence_has_word('ra82ng', 'TruyenKieu2.txt')
# test_print()
# capvan_dict = dem_capvan_lucbat('TruyenKieu2.txt', 1000(file_name='TruyenKieu.txt', lines = -1):
#     capvan_dict = {}
#     f.open(file_name, encoding='utf-8')

# group_van(capvan_dict)
# print_dict(capvan_dict, 10000)
a, b, c = count_phuam_van_dau('TruyenKieu2.txt', 100000)



# print '\n'*3
# print_dict(a, 50)
# print '\n'*3
# print_dict(b, 50)
# print '\n'*3
# print_dict(c, 50)

# aaa, bb = demchu_tu('TruyenKieu2.txt', 100000)
# print '\n'*3
# print_dict(aaa, 30)
# print '\n'*3
# print_dict(bb, 30)

# # print find_missing_sentence('TruyenKieu2.txt', 100000)

# s = 0 
# for aa in c:
#     s += c[aa];
# print s
# print len(b)
# print len(aaa)
# # print len(capvan_dict)
# print aaa['xanh']
# print aaa['thanh']
# print aaa['tai']

# # # print group_van(capvan_dict)

# # # convert2ascii_test()

# # # print 't' + code2_dict['a61'] + 'n'
# # print VNI2unicode('kho6ng')
